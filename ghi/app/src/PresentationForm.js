import React, { useEffect, useState } from 'react';

function PresentationForm() {

    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [companyName, setCompanyName] = useState('')
    const [title, setTitle] = useState('')
    const [synopsis, setSynopsis] = useState('')
    const [conference, setConference] = useState('')
    const [conferences, setConferences] = useState([])

    useEffect(() => {
        const fetchConferences = async () => {
            const url = 'http://localhost:8000/api/conferences/'
            const response = await fetch(url)
            if (response.ok) {
                const data = await response.json()
                setConferences(data.conferences)
            }
        }

        fetchConferences()

    }, [])

    const handleSubmit = async (e) => {
        e.preventDefault()
        const presentationUrl = `http://localhost:8000/${conference}presentations/`
        console.log(presentationUrl)
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify({
                presenter_name: name,
                presenter_email: email,
                company_name: companyName,
                title: title,
                synopsis: synopsis,
                conference: conference
            }),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            const newPresentation = await response.json();
            console.log(newPresentation);
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new presentation</h1>
                    <form onSubmit={handleSubmit} id="create-presentation-form">
                        <div className="form-floating mb-3">
                            <input value={name} onChange={(event) => setName(event.target.value)} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control" />
                            <label htmlFor="presenter_name">Presenter name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={email} onChange={(event) => setEmail(event.target.value)} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control" />
                            <label htmlFor="presenter_email">Presenter email</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={companyName} onChange={(event) => setCompanyName(event.target.value)} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control" />
                            <label htmlFor="company_name">Company name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={title} onChange={(event) => setTitle(event.target.value)} placeholder="Title" required type="text" name="title" id="title" className="form-control" />
                            <label htmlFor="title">Title</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="synopsis">Synopsis</label>
                            <textarea value={synopsis} onChange={(event) => setSynopsis(event.target.value)} id="synopsis" rows="3" name="synopsis" className="form-control"></textarea>
                        </div>
                        <div className="mb-3">
                            <select value={conference} onChange={(event) => setConference(event.target.value)} required name="conference" id="conference" className="form-select">
                                <option value="">Choose a conference</option>
                                {conferences.map(conference => {
                                    return <option key={conference.href} value={conference.href}>{conference.name}</option>
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default PresentationForm