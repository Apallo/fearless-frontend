import React, { useEffect, useState } from 'react';

function ConferenceForm() {
    const [name, setName] = useState('')
    const [starts, setStarts] = useState('')
    const [ends, setEnds] = useState('')
    const [description, setDescription] = useState('')
    const [maxPresentations, setMaxPresentations] = useState('')
    const [maxAttendees, setMaxAttendees] = useState('')
    const [location, setLocation] = useState('')
    const [locations, setLocations] = useState([])

    const handleSubmit = async (e) => {
        e.preventDefault()
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify({
                name: name,
                starts: starts,
                ends: ends,
                description: description,
                max_presentations: maxPresentations,
                max_attendees: maxAttendees,
                location: location
            }),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);
        }
    }

    useEffect(() => {
        const fetchLocations = async () => {
            const url = 'http://localhost:8000/api/locations/'
            const response = await fetch(url)
            const data = await response.json()
            setLocations(data.locations)
        }

        fetchLocations()
    }, [])


    return (
        <div className="my-5 container">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new location</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input value={name} onChange={(event) => setName(event.target.value)} placeholder="Name" required type="text" name="name" id="name"
                                className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={starts} onChange={(event) => setStarts(event.target.value)} placeholder="mm/dd/yyyy" required type="date" name="starts" id="starts"
                                className="form-control" />
                            <label htmlFor="start_date">Starts</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={ends} onChange={(event) => setEnds(event.target.value)} placeholder="mm/dd/yyyy" required type="date" name="ends" id="ends"
                                className="form-control" />
                            <label htmlFor="end_date">Ends</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="description" className="form-label" id="description">Description</label>
                            <textarea value={description} onChange={(event) => setDescription(event.target.value)} className="form-control" id="description" rows="3"></textarea>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={maxPresentations} onChange={(event) => setMaxPresentations(event.target.value)} placeholder="Maximum presentations" required type="number"
                                name="max_presentations" id="max_presentations" className="form-control" />
                            <label htmlFor="maximum_presentations">Maximum presentations</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={maxAttendees} onChange={(event) => setMaxAttendees(event.target.value)} placeholder="Maximum attendees" required type="number" name="max_attendees"
                                id="max_attendees" className="form-control" />
                            <label htmlFor="maximum_attendees">Maximum Attendees</label>
                        </div>
                        <div className="mb-3">
                            <select value={location} onChange={(event) => setLocation(event.target.value)} required id="location" name="location" className="form-select">
                                <option value="">Choose a location</option>
                                {locations.map(location => {
                                    return <option key={location.id} value={location.id}>{location.name}</option>
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default ConferenceForm